class Dashing.GitlabBuildStatus extends Dashing.Widget

  onData: (data) ->
    color = "#50BA5B"
    if data.running
      color = "#DA9A30"
      $(@node).find('div.gitlab-build-running').show()
    else
      $(@node).find('div.gitlab-build-running').hide()

    if data.failed
      color = "red"
      $(@node).find('div.gitlab-build-failed').show()
      $(@node).find('div.gitlab-build-succeeded').hide()
    else
      $(@node).find('div.gitlab-build-failed').hide()
      $(@node).find('div.gitlab-build-succeeded').show()

    $(@node).css("background-color", color)
