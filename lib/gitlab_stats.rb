require 'gitlab'
require 'date'

Gitlab.configure do |config|
  # API endpoint URL, default
  config.endpoint       = ENV['GITLAB_ENDPOINT']

  # User's private token or OAuth2 access token
  config.private_token  = ENV['GITLAB_TOKEN']
end

module GitlabStats
  # get a list of all projects
  def self.projects
    Gitlab.group(ENV['GITLAB_GROUP_ID']).projects.map do |proj|
      { :id => proj['id'],
        :name => proj['name'],
        :path => proj['path_with_namespace'],
        :archived => proj['archived'],
        :builds_enabled => proj['builds_enabled'],
        :default_branch => proj['default_branch']}
    end
  end

  def self.add_pipeline_stats_to(proj)
    pipelines = Gitlab.pipelines(proj[:id], { per_page: 50 })
    #pp pipeline[0]
    pipeline = pipelines.find{|p| p.ref == proj[:default_branch]}
    if pipeline.nil?
      if pipelines.any?
        proj[:status]      = 'No builds run for default branch lately'
      else
        proj[:status]      = 'No builds configured'
      end
      return
    end
    proj[:status]      = pipeline.status
    proj[:pipeline_id] = pipeline.id
    proj[:ref]         = pipeline.ref
    date               = DateTime.parse(pipeline.updated_at).strftime("%F %T")
    proj[:date]        = date
  rescue e
    proj[:status]      = "Error: #{e}"
  end

end
