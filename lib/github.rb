require 'date'
require 'open-uri'
require 'json'

class Github

  def initialize(orgname)
    url='https://api.github.com/orgs/'+orgname
    @headers={'User-Agent' => 'LEAP Dashboard'}
    if ENV.has_key?('GITHUB_AUTH_TOKEN')
      @headers['Authorization'] = "token "+ENV['GITHUB_AUTH_TOKEN']
    end
    @org=JSON.parse(open(url,@headers).read)
    @repos=JSON.parse(open(@org['repos_url'],@headers).read)
  end

  def repocount
    @repos.count
  end

  def prcount
    count=0
    @repos.each do |r|
      count+=JSON.parse(open(r['url']+'/pulls',@headers).read).count
    end
    return count
  end

  def recent_open_issues
    count=0
    @repos.each do |r|
      url=r['url']+'/issues?state=open&since='+(Time.new().to_datetime << 1).to_time.strftime("%Y-%m-%dT%H:%M:%SZ")
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def recent_issues
    count=0
    @repos.each do |r|
      url=r['url']+'/issues?state=all&since='+(Time.new().to_datetime << 1).to_time.strftime("%Y-%m-%dT%H:%M:%SZ")
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def open_pull_requests
    count=0
    @repos.each do |r|
      url=r['url']+'/pulls?state=open'
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def pull_requests
    count=0
    @repos.each do |r|
      url=r['url']+'/pulls?state=all'
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def forks
    count=0
    @repos.each do |r|
      url=r['url']+'/forks'
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def issues_in_development
    count=0
    @repos.each do |r|
      url=r['url']+'/issues?labels=2%20-%20Development'
      count+=JSON.parse(open(url,@headers).read).count
    end
    return count
  end
  def prlist
    prlist=[]
    @repos.each do |r|
      JSON.parse(open(r['url']+'/pulls',@headers).read).each do |pr|
        prlist<<{'label'=>pr['title'], 'pr_url'=>pr['html_url']}
      end
    end
    return prlist
  end

  def stargazers
    @repos.inject(0) {|sum,hash| sum + hash['stargazers_count']}
  end
  def epic_issues
    count=0
    @repos.each do |r|
        url=r['url']+'/issues?labels=UA%20multiuser'
        count+=JSON.parse(open(url,@headers).read).count
      end
    return count
  end
  def all_epic_issues
    count=0
    @repos.each do |r|
        url=r['url']+'/issues?labels=UA%20multiuser&state=all'
        count+=JSON.parse(open(url,@headers).read).count
      end
    return count
  end

  def action_items
    issues=[]
    @repos.each do |r|
      url=r['url']+'/issues?labels=Action+item'
      JSON.parse(open(url,@headers).read).each do |issue|
        title=issue['title']
        if issue.has_key?('assignee') and not issue['assignee'].nil?
          assignee=issue['assignee']['login']
        else
          assignee='None'
        end
        issues<<{'label' => title, 'value' => assignee}
      end
    end
    issues
  end
end
